# Cert-Manager with Nginx Ingress Controller

Note that the `nginx ingress controller` hast to be deployed before the `cert manager`.

    kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.6/deploy/manifests/00-crds.yaml
    kubectl label namespace ingress-nginx certmanager.k8s.io/disable-validation="true"
    helmfile -i apply
    kubectl apply -f staging-issuer.yaml
    kubectl apply -f production-issuer.yaml


# References

- https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nginx-ingress-with-cert-manager-on-digitalocean-kubernetes
- https://kubernetes.github.io/ingress-nginx/
- https://cert-manager.readthedocs.io/en/latest/index.html
