# Deploy runner

    helm tiller start-ci burkionline
    summon -p gopass helmfile -i apply

The runner registration token will be retrieved from gopass.
